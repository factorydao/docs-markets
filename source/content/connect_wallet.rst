.. _markets-vote-connect-wallet:

Connect Wallet
--------------------------
Prerequisites:

* using one of the following browsers:
    * Chrome
    * Firefox
    * Brave
    * Edge

In purpose of using an application you need to connect a wallet to the site. In the browser install an extension called *MetaMask*  and create a new account. 
From the menu bar choose import and paste there one of the Private Keys::

    0x4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d

    0x6cbed15c793ce57650b9877cf6fa156fbef513c4e6134f022a85b1ffdd59b2a1

For local use:\
    Set network to localhost

For remote server use:\
    Set network to Own RPC::

        URL RPC: http://207.154.210.94:8545/
        Chain ID: 333

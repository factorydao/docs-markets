Round Resolving
-----------------


Challenge period
^^^^^^^^^^^^^^^^
| As you know markets.vote comprises two time bounded windows, `a vote window and a market window <https://financevote.readthedocs.io/en/latest/content/whitepaper/14_vote_market.html#a-vote-market-window>`_, that usually lasts from Monday to Sunday (each).
| But there's a period of time after the winner is proposed but before it is confirmed called **challenge period**. It is a gap between market window closure at Sunday midnight UTC and winner reveal by snapshot taken. The challenge period lasts 1 day.
|
| If you're sure that true winner is not the one proposed, you can challenge by depositing 50k FVT.
| Simply click the ``challenge button`` on the ballot and say which you think is the real winner. 
| If you're right administrator will accept your challenge and your deposit will be returned. 
|
| When the challenge period passes and you want the round resolved earlier then you can call `confirmWinnerUnchallenged` and that doesn't require a deposit.
| See instructions below for that.

Instructions
^^^^^^^^^^^^^
| Anyone can call the functions on the contract `proposeWinner` and `confirmWinnerUnchallenged`:

1. Go to the etherscan link for the Markets contract: `Etherscan <https://etherscan.io/address/0xa58d366f2078900E76Bea7f3dBb64766BB9614f4>`_
2. Go to the contract tab.
3. Click on the ``Write Contract`` sub-tab.
4. ``Connect to Web3`` -> this should bring up your metamask or other browser wallet.
5. Click ``proposeWinner`` and ``write`` and confirm transaction on your wallet soon after midnight UTC Sunday-Monday.
6. If it is after midnight UTC Monday-Tuesday, then you can click ``confirmWinnerUnchallenged`` then ``write`` then confirm in your wallet.


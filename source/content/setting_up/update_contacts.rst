.. _markets-vote-update-contracts:

Update Cotracts
--------------------------
whenever deploy is being made on the testing server side, contracts must be updated:

1. Remove all contracts from *trollbox/src/truffle/build/contracts*
2. If there is no **download.txt** file, create one and put in there::

    207.154.210.94:8000/download/AggregatorInterface.json
    207.154.210.94:8000/download/AggregatorProxy.json
    207.154.210.94:8000/download/AggregatorV2V3Interface.json
    207.154.210.94:8000/download/AggregatorV3Interface.json
    207.154.210.94:8000/download/ChainLinkOracle.json
    207.154.210.94:8000/download/ChainLinkOracle2.json
    207.154.210.94:8000/download/Identity.json
    207.154.210.94:8000/download/ReferralProgram.json
    207.154.210.94:8000/download/Token.json
    207.154.210.94:8000/download/Trollbox.json
    207.154.210.94:8000/download/TrollboxProxy.json

3. run a terminal in *../contracts* folder and run::

    wget -i download.txt

It will download and copy all contracts from test server into that folder. 

\*For manual import do that for every contract::

        wget 207.154.210.94:8000/download/Contracts_Name.json

.. markets.vote documentation master file, created by
   sphinx-quickstart on Fri Jun 25 13:44:27 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to markets.vote's documentation!
========================================

`markets.vote`_  is a prediction market and collective intelligence tool, which uses quadratic voting and a decentralised identity system to curate emerging markets and reach consensus on the perceived future value of assets. It allows users to break through the noise of permissionless systems and concentrate on the assets most worthy of their attention.

.. _markets.vote: https://www.finance.vote/

.. image:: ./images/markets_dark_main.png
   :height: 100px
   :alt: docs-markets missing
   :align: center
   :target: https://www.finance.vote/

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   /content/setting_up/*
   /content/connect_wallet
   /content/round_resolving


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Our products
===============

.. |A| image:: ./images/launch_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/launch/en/latest/
   
.. |B| image:: ./images/bank_icon_dark.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |I| image:: ./images/influence_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |Y| image:: ./images/yield_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/yield/en/latest/

.. |MI| image:: ./images/mint_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/mint/en/latest/ 

* |A| `launch <https://financevote.readthedocs.io/projects/launch/en/latest/>`_
* |B| `bank <https://financevote.readthedocs.io/projects/bank/en/latest/>`_
* |I| `influence <https://financevote.readthedocs.io/projects/influence/en/latest/>`_
* |MI| `mint <https://financevote.readthedocs.io/projects/mint/en/latest/>`_
* |Y| `yield <https://financevote.readthedocs.io/projects/yield/en/latest/>`_

Back to main page
------------------
.. |H| image:: ./images/financevote_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/en/latest/

* |H| `Home <https://financevote.readthedocs.io/en/latest/>`_
